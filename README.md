# danwa-api

## Setting Up a Development Environment

1. Run Mongo Locally 

```
docker run -d --name danwa-mongo --restart unless-stopped -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=mongo -e MONGO_INITDB_ROOT_PASSWORD=mongo mongo
```

2. Set Up Environment Variables

```
export MONGO_HOST=localhost
export MONGO_USERNAME=mongo
export MONGO_PASSWORD=mongo
export MONGO_DATABASE=danwa
export TOKEN_KEY=test
```
The `.gitignore` file contains `sensitive/*`, so you can place any environment variables in a file under `sensitive/` and `source` the file.

3. Test DB Connection
```
mongo --host localhost -u mongo -p mongo --authenticationDatabase admin
```

4. Write Code and Run!
```
go run app.go
```
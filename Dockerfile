FROM golang:1.12 as builder

RUN apt update          && \
    apt install git gcc

WORKDIR /go/src/golang-api/
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app app.go
RUN touch /dummy-passwd && \
    echo "dummy-user:x:9999:9999:/:" > /dummy-passwd

FROM scratch

COPY --from=builder /go/src/golang-api/app /app
COPY --from=builder /dummy-passwd /etc/passwd

ENV MONGO_HOST localhost
ENV MONGO_USERNAME mongo
ENV MONGO_PASSWORD mongo
ENV MONGO_DATABASE test

EXPOSE 12000

USER dummy-user

CMD ["/app"]

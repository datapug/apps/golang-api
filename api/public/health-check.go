package api

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// HealthCheckRoutes API
func HealthCheckRoutes() *chi.Mux {
	router := chi.NewRouter()

	// define routes
	router.Get("/health", healthCheck)

	return router
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]string)
	response["health"] = "I'm alive!"
	render.JSON(
		w,
		r,
		response,
	)
}

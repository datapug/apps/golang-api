package db

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Todo DB object structure
type Todo struct {
	Number int
	Text   string
}

// FindTodo finds a todo based on a filter
func FindTodo(filter bson.D) *mongo.SingleResult {
	collection := db.Collection("todo")
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	res := collection.FindOne(ctx, filter)
	log.Println(res)
	return res
}

// UpdateTodo updates a todo
func UpdateTodo() {

}

// AddTodo creates a new todo
func AddTodo(t Todo) (interface{}, error) {

	collection := db.Collection("todo")
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	res, err := collection.InsertOne(ctx, t)
	id := res.InsertedID

	return id, err
}

// DeleteTodo deletes a todo
func DeleteTodo(t Todo) (interface{}, error) {

	collection := db.Collection("todos")
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	res, err := collection.InsertOne(ctx, t)
	id := res.InsertedID

	return id, err
}

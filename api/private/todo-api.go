package api

import (
	"encoding/json"
	"golang-api/db"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"go.mongodb.org/mongo-driver/bson"
)

// Todo API
func Todo() *chi.Mux {

	router := chi.NewRouter()

	// define all routes
	router.Get("/{todo}", findTodo)
	router.Put("/{todo}", updateTodo)
	router.Post("/{todo}", addTodo)
	router.Delete("/{todo}", deleteTodo)
	// ------------------ //

	return router
}

func findTodo(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]interface{})

	filter := bson.D{{Key: "number", Value: chi.URLParam(r, "todo")}}

	var result db.Todo
	db.FindTodo(filter).Decode(&result)

	out, err := json.Marshal(result)
	if err != nil {
		log.Fatal(err)
	}

	response["Todo"] = string(out)

	render.JSON(w, r, response)
}

func updateTodo(w http.ResponseWriter, r *http.Request) {

}

func addTodo(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]interface{})

	var t db.Todo

	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		log.Fatal(err)
	}

	id, err := db.AddTodo(t)
	if err != nil {
		log.Fatal(err)
	} else {
		log.Printf("Todo added: %s", id)
	}

	response["Todo"] = id

	render.JSON(w, r, response)
}

func deleteTodo(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]string)

	render.JSON(w, r, response)
}

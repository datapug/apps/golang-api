package db

import (
	"context"
	"fmt"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var db *mongo.Database

func init() {
	db = connect()
}

func connect() *mongo.Database {

	dbURI := fmt.Sprintf(
		"mongodb://%s:%s@%s:27017/?authSource=admin",
		os.Getenv("MONGO_USERNAME"),
		os.Getenv("MONGO_PASSWORD"),
		os.Getenv("MONGO_HOSTNAME"),
	)

	log.Println("Connecting to database...")
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(dbURI))

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	} else {
		log.Println("Connected to MongoDB!")
	}

	return client.Database(os.Getenv("MONGO_DATABASE"))
}

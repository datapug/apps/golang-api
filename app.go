package main

import (
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"

	prvAPI "golang-api/api/private"
	pubAPI "golang-api/api/public"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"
)

func main() {

	router := routes()

	walkFunc := func(
		method string,
		route string,
		handler http.Handler,
		middlewares ...func(http.Handler) http.Handler,
	) error {
		log.Printf("%s %s\n", method, route) // Walk and print out all routes
		return nil
	}

	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("Logging err: %s\n", err.Error()) // panic if error
	}

	log.Fatal(http.ListenAndServe(":8080", router))
}

func routes() *chi.Mux {
	router := chi.NewRouter()

	// testing ~~~~~ //
	tokenAuth := jwtauth.New("HS256", []byte("test"), nil)
	claims := map[string]interface{}{
		"roles": []string{"user"},
		"user":  "banana",
	}
	_, token, _ := tokenAuth.Encode(jwt.MapClaims(claims))
	log.Println(token)
	// ~~~~~~~~~~~~~ //

	router.Use(
		render.SetContentType(render.ContentTypeJSON), // Set Content-Type headers to always JSON
		middleware.Logger,          // Log API requests
		middleware.DefaultCompress, // Compress results - gzipping/json
		middleware.RedirectSlashes, // Redirect slashes to no slash URL versions
		middleware.Recoverer,       // Recover from panics without crashing server
	)

	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped.
	router.Use(middleware.Timeout(30 * time.Second))

	// PROTECTED ENDPOINTS
	router.Group(func(r chi.Router) {
		// Use JWT Authentication
		r.Use(
			// Seek, Verify, and Validate tokens
			jwtauth.Verifier(
				tokenAuth,
			),
			// Handle valid/invalid tokens
			jwtauth.Authenticator,
		)

		r.Route("/api", func(r chi.Router) {
			r.Mount("/v1/todo", prvAPI.Todo())
		})
	})

	// PUBLIC ENDPOINTS - No Authorization Needed
	router.Group(func(r chi.Router) {
		r.Mount("/", pubAPI.HealthCheckRoutes())
	})

	return router
}
